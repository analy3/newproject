#!/bin/sh

echo "What is the name of the project? "
read project
cd ../
if [ -d $project ]; then
    echo "$project project already exists..."
    exit
fi
if [ ! -d templates/project ]; then
    echo "template 'project' does not exist..."
    exit
fi

cp -r templates/project $project
cd $project
if [ -d .git ]; then rm -rf .git; fi
if [ -f project.sh ]; then mv project.sh $project.sh; fi
if [ -f sql/project.sql ]; then
    mv sql/project.sql sql/$project.sql
    echo "Enter sudo password AND root password to CREATE DATABASE"
    sudo mysql -u root -p -e "CREATE DATABASE IF NOT EXISTS $project DEFAULT CHARACTER SET = 'utf8' DEFAULT COLLATE 'utf8_general_ci'"
    capitalized=$(echo "$project" | tr '[:lower:]' '[:upper:]')
    sed -i 's/\*project/'$capitalized'/g' sql/$project.sql
    sed -i 's/USE project/USE '$project'/g' sql/$project.sql
fi